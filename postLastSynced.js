import * as dynamoDbLib from "./libs/dynamodb-lib";
import { success, failure } from "./libs/response-lib";

export async function main(event, context, callback) {
    const data = JSON.parse(event.body);

    const params = {
        TableName: "UserInfo",
        Key: {
            userId: data.userId
        },
        UpdateExpression: "SET lastSynced  = :lastSynced",
        ExpressionAttributeValues: { 
            ":lastSynced": data.lastSynced ? data.lastSynced : null
        },
        ReturnValues: "ALL_NEW"
    };

    try {
        const updateResult = await dynamoDbLib.call("update", params);
        callback(null, success({status: "Updated last sync time", data: updateResult}));
    } catch (e) {
        callback(null, failure({ status: "Failed to update last sync time", userId: data.userId}));
    }
}