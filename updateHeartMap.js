import * as dynamoDbLib from "./libs/dynamodb-lib";
import { success, failure } from "./libs/response-lib";

export async function main(event, context, callback) {
  const data = JSON.parse(event.body);

  const getParams = {
    TableName: "HeartRate",
    Key: {
      userId: data.userId
    }
  };

  try {
    const result = await dynamoDbLib.call("get", getParams);
    let heartMap = data.map;
    if (result.Item.HRMap) {
      const prevHeartMap = result.Item.HRMap;
      heartMap = Object.assign(prevHeartMap, data.map);
    } 

    const params = {
        TableName: "HeartRate",
        Key: {
            userId: data.userId
        },
        UpdateExpression: "SET HRMap  = :heartMap",
        ExpressionAttributeValues: { 
            ":heartMap": heartMap ? heartMap : null
        },
        ReturnValues: "ALL_NEW"
      };

      try {
        const updateResult = await dynamoDbLib.call("update", params);
        callback(null, success({status: true, message: "Appended to heart map successfully"}));
      } catch (e) {
        callback(null, failure({ status: false, message: "Failed to append to heart map" }));
      }

  } catch (e) {
    callback(null, failure({ status: false, message: "Failed to retrieve previous heart map"}));
  }
}