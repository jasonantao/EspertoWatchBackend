import * as dynamoDbLib from "./libs/dynamodb-lib";
import { success, failure } from "./libs/response-lib";

export async function main(event, context, callback) {
  const data = JSON.parse(event.body);
  const params = {
    TableName: "StepCountGoals",
    Item: data
  };

  try {
    await dynamoDbLib.call("put", params);
    callback(null, success({status: true, message: "Posted to step count goals successfully"}));
  } catch (e) {
    callback(null, failure({ status: false, message: "Error posting to step count goals" }));
  }
}