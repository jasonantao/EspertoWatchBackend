import * as dynamoDbLib from "./libs/dynamodb-lib";
import { success, failure } from "./libs/response-lib";

export async function main(event, context, callback) {
  const data = JSON.parse(event.body);

  const getParams = {
    TableName: "StepCount",
    Key: {
      userId: data.userId
    }
  };

  try {
    const result = await dynamoDbLib.call("get", getParams);
    let stepsMap = data.map;
    if (result.Item.stepsMap) {
      const prevStepsMap = result.Item.stepsMap;
      stepsMap = Object.assign(prevStepsMap, data.map);
    } 

    const params = {
        TableName: "StepCount",
        Key: {
            userId: data.userId
        },
        UpdateExpression: "SET stepsMap = :stepsMap",
        ExpressionAttributeValues: { 
            ":stepsMap": stepsMap ? stepsMap : null
        },
        ReturnValues: "ALL_NEW"
      };

      try {
        const updateResult = await dynamoDbLib.call("update", params);
        callback(null, success({status: true, message: "Appended to steps map successfully"}));
      } catch (e) {
        callback(null, failure({ status: false, message: "Failed to append to steps map" }));
      }

  } catch (e) {
    callback(null, failure({ status: false, message: "Failed to retrieve previous steps map"}));
  }
}